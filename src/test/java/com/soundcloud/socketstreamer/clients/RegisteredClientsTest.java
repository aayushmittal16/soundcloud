package com.soundcloud.socketstreamer.clients;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.soundcloud.socketstreamer.Configurations;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class RegisteredClientsTest {

  private final Map<Integer, Client> registeredClientsDb =
      new HashMap<>(Configurations.getInstance().getConcurrencyLevel());

  private RegisteredClients registry;

  @Before
  public void prepareTest() {
    registry = RegisteredClients.getInstance();
    Whitebox.setInternalState(registry, Map.class, registeredClientsDb);

    Client client1 = mock(Client.class);
    when(client1.getId()).thenReturn(1);
    Client client2 = mock(Client.class);
    when(client2.getId()).thenReturn(2);
    Client client3 = mock(Client.class);
    when(client3.getId()).thenReturn(3);
    Client client4 = mock(Client.class);
    when(client4.getId()).thenReturn(4);
    registeredClientsDb.put(client1.getId(), client1);
    registeredClientsDb.put(client2.getId(), client2);
    registeredClientsDb.put(client3.getId(), client3);
    registeredClientsDb.put(client4.getId(), client4);
  }

  @Test
  public void testGetClient() {
    assertEquals(registry.getClient(1), registeredClientsDb.get(1));
  }

  @Test
  public void testGetClients() {
    Set<Integer> clients = new HashSet<>(Arrays.asList(1, 2, 3));
    Collection<Client> result = registry.getClients(clients);
    assertEquals(result.size(), clients.size());
    for (Client c : result) {
      assertTrue(clients.contains(c.getId()));
    }
  }

  @Test
  public void testGetAllClients() {
    Collection<Client> clients = registry.getAllClients();
    assertEquals(clients.size(), registeredClientsDb.size());
    for (Client c : clients) {
      assertTrue(registeredClientsDb.containsKey(c.getId()));
    }
  }

  @Test
  public void testRegisterClient() {
    Client client = mock(Client.class);
    when(client.getId()).thenReturn(5);
    registry.registerClient(client);
    assertTrue(registeredClientsDb.containsKey(client.getId()));
  }

  @Test
  public void testGetNumOfConnections() {
    assertEquals(registry.getNumofConnections(), registeredClientsDb.size());
  }
}
