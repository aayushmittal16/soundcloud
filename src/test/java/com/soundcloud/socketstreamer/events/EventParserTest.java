package com.soundcloud.socketstreamer.events;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.soundcloud.socketstreamer.events.models.BroadcastEvent;
import com.soundcloud.socketstreamer.events.models.Event;
import com.soundcloud.socketstreamer.events.models.FollowEvent;
import com.soundcloud.socketstreamer.events.models.PrivateMessageEvent;
import com.soundcloud.socketstreamer.events.models.StatusUpdateEvent;
import com.soundcloud.socketstreamer.events.models.UnfollowEvent;
import org.junit.Test;

public class EventParserTest {

  @Test
  public void testBroadcastEventParser() {
    String payload = "542532|B";
    Event event = EventParser.parseEvent(payload);
    assertTrue(event instanceof BroadcastEvent);
    BroadcastEvent broadcastEvent = (BroadcastEvent) event;
    assertEquals(542532, broadcastEvent.getSequence());
    assertEquals(payload, broadcastEvent.getPayload());
  }

  @Test
  public void testFollowEventParser() {
    String payload = "666|F|60|50";
    Event event = EventParser.parseEvent(payload);
    assertTrue(event instanceof FollowEvent);
    FollowEvent followEvent = (FollowEvent) event;
    assertEquals(666, followEvent.getSequence());
    assertEquals(payload, followEvent.getPayload());
    assertEquals(60, followEvent.getFrom());
    assertEquals(50, followEvent.getTo());
  }

  @Test
  public void testUnfollowEventParser() {
    String payload = "1|U|12|9";
    Event event = EventParser.parseEvent(payload);
    assertTrue(event instanceof UnfollowEvent);
    UnfollowEvent unfollowEvent = (UnfollowEvent) event;
    assertEquals(1, unfollowEvent.getSequence());
    assertEquals(payload, unfollowEvent.getPayload());
    assertEquals(12, unfollowEvent.getFrom());
    assertEquals(9, unfollowEvent.getTo());
  }

  @Test
  public void testPrivateMessageEventParser() {
    String payload = "43|P|32|56";
    Event event = EventParser.parseEvent(payload);
    assertTrue(event instanceof PrivateMessageEvent);
    PrivateMessageEvent privateMessageEvent = (PrivateMessageEvent) event;
    assertEquals(43, privateMessageEvent.getSequence());
    assertEquals(payload, privateMessageEvent.getPayload());
    assertEquals(32, privateMessageEvent.getFrom());
    assertEquals(56, privateMessageEvent.getTo());
  }

  @Test
  public void testStatusUpdateEventParser() {
    String payload = "634|S|32";
    Event event = EventParser.parseEvent(payload);
    assertTrue(event instanceof StatusUpdateEvent);
    StatusUpdateEvent statusUpdateEvent = (StatusUpdateEvent) event;
    assertEquals(634, statusUpdateEvent.getSequence());
    assertEquals(payload, statusUpdateEvent.getPayload());
    assertEquals(32, statusUpdateEvent.getFrom());
  }
}
