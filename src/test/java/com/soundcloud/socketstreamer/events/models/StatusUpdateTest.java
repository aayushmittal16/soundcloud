package com.soundcloud.socketstreamer.events.models;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;

public class StatusUpdateTest extends EventTest {

  private StatusUpdateEvent event = new StatusUpdateEvent(634, "634|S|3", 3);

  @Test
  public void testApply() {
    assertTrue(db.contains(3));
    db.get(3).addFollower(2);
    db.get(3).addFollower(1);
    event.apply();
    verify(client, times(1)).write(event.getPayload());
    verify(client2, times(1)).write(event.getPayload());
    verify(client3, times(0)).write(event.getPayload());
  }
}
