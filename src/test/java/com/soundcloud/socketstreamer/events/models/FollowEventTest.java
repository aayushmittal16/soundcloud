package com.soundcloud.socketstreamer.events.models;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;

public class FollowEventTest extends EventTest {

  private FollowEvent event = new FollowEvent(666, "666|F|1|2", 1, 2);

  @Test
  public void testApply() {
    assertFalse(db.get(2).getFollowers().contains(1));
    event.apply();
    assertTrue(db.get(2).getFollowers().contains(1));
    verify(client, times(0)).write(event.getPayload());
    verify(client2, times(1)).write(event.getPayload());
  }
}
