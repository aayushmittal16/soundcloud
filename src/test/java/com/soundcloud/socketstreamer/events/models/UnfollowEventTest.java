package com.soundcloud.socketstreamer.events.models;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class UnfollowEventTest extends EventTest {

  private UnfollowEvent event = new UnfollowEvent(213, "213|U|1|2", 1, 2);

  @Test
  public void testApply() {
    assertTrue(db.contains(2));
    db.get(2).addFollower(1);
    event.apply();
    assertFalse(db.get(2).getFollowers().contains(1));
  }
}
