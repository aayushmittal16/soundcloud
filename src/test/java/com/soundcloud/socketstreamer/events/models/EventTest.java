package com.soundcloud.socketstreamer.events.models;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.soundcloud.socketstreamer.clients.Client;
import com.soundcloud.socketstreamer.clients.RegisteredClients;
import com.soundcloud.socketstreamer.users.UserDb;
import org.junit.Before;

public class EventTest {

  UserDb db = UserDb.getInstance();
  Client client;
  Client client2;
  Client client3;
  private RegisteredClients registeredClientsDb = RegisteredClients.getInstance();

  @Before
  public void init() {
    client = mock(Client.class);
    client2 = mock(Client.class);
    client3 = mock(Client.class);
    when(client.getId()).thenReturn(1);
    when(client2.getId()).thenReturn(2);
    when(client3.getId()).thenReturn(3);
    registeredClientsDb.registerClient(client);
    registeredClientsDb.registerClient(client2);
    registeredClientsDb.registerClient(client3);
    db.add(1);
    db.add(2);
    db.add(3);
  }
}
