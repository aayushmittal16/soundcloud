package com.soundcloud.socketstreamer.events.models;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;

public class BroadcastEventTest extends EventTest {

  private BroadcastEvent event = new BroadcastEvent(22, "22|B");

  @Test
  public void testApply() {
    event.apply();
    verify(client, times(1)).write(event.getPayload());
    verify(client2, times(1)).write(event.getPayload());
    verify(client3, times(1)).write(event.getPayload());
  }
}
