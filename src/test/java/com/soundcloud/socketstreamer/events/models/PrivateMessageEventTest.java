package com.soundcloud.socketstreamer.events.models;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;

public class PrivateMessageEventTest extends EventTest {

  private PrivateMessageEvent event = new PrivateMessageEvent(43, "43|P|1|2", 1, 2);

  @Test
  public void testApply() {
    event.apply();
    verify(client, times(0)).write(event.getPayload());
    verify(client2, times(1)).write(event.getPayload());
  }
}
