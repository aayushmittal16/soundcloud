package com.soundcloud.socketstreamer.users;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.soundcloud.socketstreamer.Configurations;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class UserDbTest {

  private Map<Integer, User> db = new HashMap<>(Configurations.getInstance().getNumberOfUsers());
  private UserDb userDb;

  @Before
  public void init() {
    userDb = UserDb.getInstance();
    Whitebox.setInternalState(userDb, Map.class, db);

    User user = new User(1);
    db.put(user.getId(), user);
  }

  @Test
  public void testContains() {
    assertFalse(userDb.contains(2));
    assertTrue(userDb.contains(1));
  }

  @Test
  public void testGet() {
    assertTrue(db.containsKey(1));
    assertFalse(db.containsKey(2));
  }

  public void testAdd() {
    userDb.add(3);
    assertTrue(db.containsKey(3));
  }
}
