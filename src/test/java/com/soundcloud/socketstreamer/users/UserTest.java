package com.soundcloud.socketstreamer.users;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class UserTest {

  private Set<Integer> followers = new HashSet<>();
  private User user;

  @Before
  public void init() {
    user = new User(1);
    Whitebox.setInternalState(user, Set.class, followers);

    followers.add(5);
  }

  @Test
  public void testAddFollower() {
    user.addFollower(2);
    assertTrue(user.getFollowers().contains(2));
  }

  @Test
  public void testRemoveFollower() {
    user.removeFollower(5);
    assertFalse(user.getFollowers().contains(5));
  }
}
