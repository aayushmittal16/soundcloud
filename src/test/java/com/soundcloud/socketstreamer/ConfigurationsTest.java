package com.soundcloud.socketstreamer;

import static org.junit.Assert.assertEquals;

import java.util.logging.Level;
import org.junit.Test;

public class ConfigurationsTest {

  @Test
  public void test_configuration_parser() {
    String[] args = {
      "logLevel",
      "severe",
      "eventListenerPort",
      "9091",
      "clientListenerPort",
      "9092",
      "concurrencyLevel",
      "1000",
      "numberOfUsers",
      "10000",
      "timeout",
      "10000",
      "maxEventSourceBatchSize",
      "1000",
      "logInterval",
      "1500"
    };
    Configurations config = Configurations.parseArgs(args);
    assertEquals(Level.SEVERE, config.getLogLevel());
    assertEquals(9091, config.getEventListenerPort());
    assertEquals(9092, config.getClientListenerPort());
    assertEquals(1000, config.getConcurrencyLevel());
    assertEquals(10000, config.getNumberOfUsers());
    assertEquals(10000, config.getTimeout());
    assertEquals(1000, config.getMaxEventSourceBatchSize());
  }
}
