package com.soundcloud.socketstreamer;

import com.soundcloud.socketstreamer.clients.ClientSocketListener;
import com.soundcloud.socketstreamer.clients.RegisteredClients;
import com.soundcloud.socketstreamer.events.EventSourceSocketListener;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

  public static void main(String[] args) {
    Configurations config = Configurations.parseArgs(args);
    Logger.getLogger("").getHandlers()[0].setLevel(config.getLogLevel());
    Logger logger = LoggerFactory.createLogger(Main.class);
    logger.info("Starting the application...");
    logger.log(
        Level.INFO,
        "Now accepting client connections on port {0}",
        String.valueOf(config.getClientListenerPort()));
    new Thread(
            new Runnable() {
              @Override
              public void run() {
                ClientSocketListener.listen();
              }
            })
        .start();
    logger.log(
        Level.INFO,
        "Now listening to event source on port {0}",
        String.valueOf(config.getEventListenerPort()));
    EventSourceSocketListener.listen();
    RegisteredClients.getInstance().shutdown();
    logger.info("All registered clients have now been terminated...");
  }
}
