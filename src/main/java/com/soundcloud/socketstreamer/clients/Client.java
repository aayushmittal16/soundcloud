package com.soundcloud.socketstreamer.clients;

import com.soundcloud.socketstreamer.LoggerFactory;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {
  private static final String CHARSET = StandardCharsets.UTF_8.name();
  private final Logger logger;
  private final Socket socket;
  private final int id;
  private final Scanner scanner;
  private final BufferedOutputStream outputStream;

  public Client(Socket socket) throws IOException {
    this.socket = socket;
    scanner = new Scanner(socket.getInputStream(), CHARSET);
    outputStream = new BufferedOutputStream(socket.getOutputStream());
    logger = LoggerFactory.createLogger(Client.class);
    id = parseId();
  }

  public int getId() {
    return id;
  }

  private int parseId() {
    String line = scanner.hasNextLine() ? scanner.nextLine().trim() : "";
    logger.log(Level.FINE, "Received id {0} from a newly registered client", line);
    return Integer.valueOf(line);
  }

  public void write(String payload) {
    try {
      String line = payload + "\r\n";
      outputStream.write(line.getBytes());
      outputStream.flush();
      logger.log(
          Level.FINE,
          "Payload \"{0}\" written to client {1}",
          new String[] {payload, String.valueOf(id)});
    } catch (IOException e) {
      logger.log(
          Level.SEVERE,
          "Unable to write payload \"{0}\" to client {1}. Cause: {2}",
          new String[] {payload, String.valueOf(id), e.getMessage()});
    }
  }

  public void close() {
    try {
      scanner.close();
      outputStream.close();
      socket.close();
      logger.log(
          Level.INFO,
          "Client connection for client: {0} has been successfully closed",
          String.valueOf(id));
    } catch (IOException e) {
      logger.log(
          Level.SEVERE,
          "Unable to close down the client connection for client: {0}. Cause: {1}",
          new String[] {String.valueOf(id), e.getMessage()});
    }
  }

  @Override
  public int hashCode() {
    return id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Client client = (Client) o;
    return id == client.id;
  }
}
