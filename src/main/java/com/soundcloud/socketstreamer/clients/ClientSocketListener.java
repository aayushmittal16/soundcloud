package com.soundcloud.socketstreamer.clients;

import com.soundcloud.socketstreamer.Configurations;
import com.soundcloud.socketstreamer.LoggerFactory;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientSocketListener {

  private static Logger logger;

  private ClientSocketListener() {}

  public static void listen() {
    getLogger();
    final int port = Configurations.getInstance().getClientListenerPort();
    try (ServerSocket server = new ServerSocket(port)) {
      logger.log(Level.INFO, "Listening to client connections on port {0}", String.valueOf(port));
      server.setSoTimeout(Configurations.getInstance().getTimeout());
      Socket connectedClient;
      while ((connectedClient = server.accept()) != null) {
        Client client = new Client(connectedClient);
        RegisteredClients.getInstance().registerClient(client);
        logger.log(
            Level.INFO,
            "Client {0} has joined the server on port {1}",
            new String[] {String.valueOf(client.getId()), String.valueOf(port)});
      }
      logger.log(
          Level.INFO,
          "Connected with {0} clients on port {1}",
          new String[] {
            String.valueOf(RegisteredClients.getInstance().getNumofConnections()),
            String.valueOf(port)
          });
    } catch (IOException e) {
      logger.log(
          Level.SEVERE,
          "Error communicating with a client on port {0}. Cause: {1}",
          new String[] {String.valueOf(port), e.getMessage()});
    }
  }

  private static void getLogger() {
    if (logger == null) {
      logger = LoggerFactory.createLogger(ClientSocketListener.class);
    }
  }
}
