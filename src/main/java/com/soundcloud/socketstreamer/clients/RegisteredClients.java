package com.soundcloud.socketstreamer.clients;

import com.soundcloud.socketstreamer.Configurations;
import com.soundcloud.socketstreamer.LoggerFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RegisteredClients {

  private static final RegisteredClients INSTANCE = new RegisteredClients();
  private final Logger logger;
  private final Map<Integer, Client> registeredClientsDb;

  private RegisteredClients() {
    logger = LoggerFactory.createLogger(RegisteredClients.class);
    registeredClientsDb = new HashMap<>(Configurations.getInstance().getConcurrencyLevel());
  }

  public static RegisteredClients getInstance() {
    return INSTANCE;
  }

  public synchronized Client getClient(int id) {
    return registeredClientsDb.get(id);
  }

  public synchronized Collection<Client> getAllClients() {
    return Collections.unmodifiableCollection(registeredClientsDb.values());
  }

  public synchronized Collection<Client> getClients(Set<Integer> clientIds) {
    List<Client> clients = new ArrayList<>();
    for (int id : clientIds) {
      if (registeredClientsDb.containsKey(id)) {
        clients.add(registeredClientsDb.get(id));
      }
    }
    return Collections.unmodifiableCollection(clients);
  }

  public synchronized int getNumofConnections() {
    return registeredClientsDb.size();
  }

  public synchronized void registerClient(Client client) {
    registeredClientsDb.put(client.getId(), client);
  }

  public void shutdown() {
    logger.info("Shutting down all client connections with the server");
    for (Client client : registeredClientsDb.values()) {
      client.close();
      logger.log(
          Level.FINE,
          "Client {0} connection has now been terminated",
          String.valueOf(client.getId()));
    }
    logger.info("All client connections with the server have been terminated. Shutdown successful");
  }
}
