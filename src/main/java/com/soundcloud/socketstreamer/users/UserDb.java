package com.soundcloud.socketstreamer.users;

import com.soundcloud.socketstreamer.Configurations;
import java.util.HashMap;
import java.util.Map;

public class UserDb {
  private static final UserDb INSTANCE = new UserDb();
  private Map<Integer, User> db;

  private UserDb() {
    db = new HashMap<>(Configurations.getInstance().getNumberOfUsers());
  }

  public static UserDb getInstance() {
    return INSTANCE;
  }

  public synchronized User get(int id) {
    return db.get(id);
  }

  public synchronized void add(int id) {
    db.put(id, new User(id));
  }

  public synchronized boolean contains(int id) {
    return db.containsKey(id);
  }
}
