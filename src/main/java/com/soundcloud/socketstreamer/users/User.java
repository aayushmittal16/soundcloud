package com.soundcloud.socketstreamer.users;

import java.util.HashSet;
import java.util.Set;

public class User {
  private int id;
  private Set<Integer> followers;

  public User(int id) {
    this.id = id;
    this.followers = new HashSet<>();
  }

  public int getId() {
    return id;
  }

  public Set<Integer> getFollowers() {
    return followers;
  }

  public void addFollower(int user) {
    followers.add(user);
  }

  public void removeFollower(int user) {
    followers.remove(user);
  }
}
