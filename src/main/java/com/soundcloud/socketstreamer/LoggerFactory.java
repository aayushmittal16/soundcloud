package com.soundcloud.socketstreamer;

import java.util.logging.Logger;

public class LoggerFactory {

  private LoggerFactory() {}

  public static Logger createLogger(Class classObj) {
    Logger logger = java.util.logging.Logger.getLogger(classObj.getName());
    logger.setLevel(Configurations.getInstance().getLogLevel());
    return logger;
  }
}
