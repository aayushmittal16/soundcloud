package com.soundcloud.socketstreamer;

import java.util.logging.Level;

public class Configurations {

  private static final Configurations INSTANCE = new Configurations();
  private Level logLevel = Level.INFO;
  private int eventListenerPort = 9090;
  private int clientListenerPort = 9099;
  private int concurrencyLevel = 100;
  private int numberOfUsers = 1000;
  private int timeout = 20000;
  private int maxEventSourceBatchSize = 100;

  private Configurations() {}

  public static Configurations getInstance() {
    return INSTANCE;
  }

  public static Configurations parseArgs(String[] args) {
    // No configurations present or invalid key/value pairs
    if (args.length == 0 || args.length % 2 == 1) {
      return INSTANCE;
    }

    for (int i = 0; i < args.length; i = i + 2) {
      switch (args[i]) {
        case "logLevel":
          INSTANCE.logLevel = parseLogLevel(args[i + 1]);
          break;
        case "eventListenerPort":
          INSTANCE.eventListenerPort = Integer.valueOf(args[i + 1]);
          break;
        case "clientListenerPort":
          INSTANCE.clientListenerPort = Integer.valueOf(args[i + 1]);
          break;
        case "concurrencyLevel":
          INSTANCE.concurrencyLevel = Integer.valueOf(args[i + 1]);
          break;
        case "numberOfUsers":
          INSTANCE.numberOfUsers = Integer.valueOf(args[i + 1]);
          break;
        case "timeout":
          INSTANCE.timeout = Integer.valueOf(args[i + 1]);
          break;
        case "maxEventSourceBatchSize":
          INSTANCE.maxEventSourceBatchSize = Integer.valueOf(args[i + 1]);
        default:
      }
    }
    return INSTANCE;
  }

  private static Level parseLogLevel(String name) {
    name = name.toUpperCase();
    Level level;
    try {
      level = Level.parse(name);
      level = level == Level.FINE || level == Level.SEVERE ? level : Level.INFO;
    } catch (IllegalArgumentException e) {
      level = Level.INFO;
    }
    return level;
  }

  public Level getLogLevel() {
    return logLevel;
  }

  public int getEventListenerPort() {
    return eventListenerPort;
  }

  public int getClientListenerPort() {
    return clientListenerPort;
  }

  public int getConcurrencyLevel() {
    return concurrencyLevel;
  }

  public int getNumberOfUsers() {
    return numberOfUsers;
  }

  public int getTimeout() {
    return timeout;
  }

  public int getMaxEventSourceBatchSize() {
    return maxEventSourceBatchSize;
  }
}
