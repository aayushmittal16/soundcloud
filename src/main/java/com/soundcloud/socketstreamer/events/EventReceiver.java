package com.soundcloud.socketstreamer.events;

import com.soundcloud.socketstreamer.Configurations;
import com.soundcloud.socketstreamer.LoggerFactory;
import com.soundcloud.socketstreamer.events.models.Event;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EventReceiver {

  private final Logger logger;
  private int sequence = 1;
  private PriorityQueue<Event> events;

  public EventReceiver() {
    logger = LoggerFactory.createLogger(EventReceiver.class);
    this.events =
        new PriorityQueue<>(
            Configurations.getInstance().getMaxEventSourceBatchSize(),
            new Comparator<Event>() {
              @Override
              public int compare(Event o1, Event o2) {
                return o1.getSequence() - o2.getSequence();
              }
            });
  }

  public void registerEvents(BufferedReader reader) throws IOException {
    String payload;
    while ((payload = reader.readLine()) != null) {
      process(EventParser.parseEvent(payload));
    }
  }

  private void process(Event e) {
    if (e == null) {
      return;
    }
    events.add(e);
    logger.log(
        Level.FINE, "Event {0} has been queued for processing", String.valueOf(e.getSequence()));
    while (!events.isEmpty() && events.peek().getSequence() == sequence) {
      Event event = events.poll();
      event.apply();
      logger.log(
          Level.FINE,
          "Event {0} has been processed and removed from the queue",
          String.valueOf(e.getSequence()));
      sequence++;
    }
  }
}
