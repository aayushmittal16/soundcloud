package com.soundcloud.socketstreamer.events;

import com.soundcloud.socketstreamer.events.models.BroadcastEvent;
import com.soundcloud.socketstreamer.events.models.Event;
import com.soundcloud.socketstreamer.events.models.FollowEvent;
import com.soundcloud.socketstreamer.events.models.PrivateMessageEvent;
import com.soundcloud.socketstreamer.events.models.StatusUpdateEvent;
import com.soundcloud.socketstreamer.events.models.UnfollowEvent;

class EventParser {

  private EventParser() {}

  static Event parseEvent(String line) {
    String[] arr = line.split("\\|");
    if (arr.length < 2) {
      return null;
    }
    int sequence = Integer.parseInt(arr[0]);
    char type = arr[1].charAt(0);
    if (arr.length == 2) {
      return new BroadcastEvent(sequence, line);
    }
    int from = Integer.parseInt(arr[2]);
    if (arr.length == 3) {
      return new StatusUpdateEvent(sequence, line, from);
    }
    int to = Integer.parseInt(arr[3]);
    Event event;
    switch (type) {
      case 'F':
        event = new FollowEvent(sequence, line, from, to);
        break;
      case 'U':
        event = new UnfollowEvent(sequence, line, from, to);
        break;
      case 'P':
        event = new PrivateMessageEvent(sequence, line, from, to);
        break;
      default:
        event = null;
    }
    return event;
  }
}
