package com.soundcloud.socketstreamer.events;

import com.soundcloud.socketstreamer.Configurations;
import com.soundcloud.socketstreamer.LoggerFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EventSourceSocketListener {

  private static Logger logger;

  private EventSourceSocketListener() {}

  public static void listen() {
    getLogger();
    EventReceiver eventReceiver = new EventReceiver();
    final int port = Configurations.getInstance().getEventListenerPort();
    try (ServerSocket server = new ServerSocket(port)) {
      logger.log(
          Level.INFO, "Listening to event source connection on port {0}", String.valueOf(port));
      server.setSoTimeout(Configurations.getInstance().getTimeout());
      Socket connectedClient;
      logger.info("Registering events for processing...");
      while ((connectedClient = server.accept()) != null) {
        BufferedReader reader =
            new BufferedReader(
                new InputStreamReader(
                    connectedClient.getInputStream(), StandardCharsets.UTF_8.name()));
        eventReceiver.registerEvents(reader);
        reader.close();
      }
    } catch (IOException e) {
      logger.log(
          Level.SEVERE,
          "An error occurred listening to event source on port {0}. Cause: {1}",
          new String[] {String.valueOf(port), e.getMessage()});
    }
  }

  private static void getLogger() {
    if (logger == null) {
      logger = LoggerFactory.createLogger(EventSourceSocketListener.class);
    }
  }
}
