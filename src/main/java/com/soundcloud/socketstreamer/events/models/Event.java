package com.soundcloud.socketstreamer.events.models;

public abstract class Event {

  private final int sequence;
  private final String payload;

  public Event(int sequence, String payload) {
    this.sequence = sequence;
    this.payload = payload;
  }

  public int getSequence() {
    return sequence;
  }

  public String getPayload() {
    return payload;
  }

  public abstract void apply();
}
