package com.soundcloud.socketstreamer.events.models;

import com.soundcloud.socketstreamer.clients.Client;
import com.soundcloud.socketstreamer.clients.RegisteredClients;

public class BroadcastEvent extends Event {

  public BroadcastEvent(int sequence, String payload) {
    super(sequence, payload);
  }

  @Override
  public void apply() {
    for (Client client : RegisteredClients.getInstance().getAllClients()) {
      client.write(getPayload());
    }
  }
}
