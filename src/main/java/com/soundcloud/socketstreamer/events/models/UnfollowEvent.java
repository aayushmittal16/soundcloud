package com.soundcloud.socketstreamer.events.models;

import com.soundcloud.socketstreamer.users.UserDb;

public class UnfollowEvent extends Event {

  private int from;
  private int to;

  public UnfollowEvent(int sequence, String payload, int from, int to) {
    super(sequence, payload);
    this.from = from;
    this.to = to;
  }

  @Override
  public void apply() {
    UserDb db = UserDb.getInstance();
    if (!db.contains(to)) {
      db.add(to);
    }
    db.get(to).removeFollower(from);
  }

  public int getFrom() {
    return from;
  }

  public int getTo() {
    return to;
  }
}
