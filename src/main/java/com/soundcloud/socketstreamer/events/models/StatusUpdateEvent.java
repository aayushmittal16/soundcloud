package com.soundcloud.socketstreamer.events.models;

import com.soundcloud.socketstreamer.clients.Client;
import com.soundcloud.socketstreamer.clients.RegisteredClients;
import com.soundcloud.socketstreamer.users.UserDb;
import java.util.Set;

public class StatusUpdateEvent extends Event {

  private int from;

  public StatusUpdateEvent(int sequence, String payload, int from) {
    super(sequence, payload);
    this.from = from;
  }

  @Override
  public void apply() {
    UserDb db = UserDb.getInstance();
    if (!db.contains(from)) {
      db.add(from);
    }
    Set<Integer> followerIds = db.get(from).getFollowers();
    for (Client client : RegisteredClients.getInstance().getClients(followerIds)) {
      client.write(getPayload());
    }
  }

  public int getFrom() {
    return from;
  }
}
