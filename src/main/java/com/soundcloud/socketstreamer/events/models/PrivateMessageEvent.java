package com.soundcloud.socketstreamer.events.models;

import com.soundcloud.socketstreamer.clients.Client;
import com.soundcloud.socketstreamer.clients.RegisteredClients;

public class PrivateMessageEvent extends Event {

  private int from;
  private int to;

  public PrivateMessageEvent(int sequence, String payload, int from, int to) {
    super(sequence, payload);
    this.from = from;
    this.to = to;
  }

  @Override
  public void apply() {
    Client client = RegisteredClients.getInstance().getClient(to);
    if (client != null) {
      client.write(getPayload());
    }
  }

  public int getFrom() {
    return from;
  }

  public int getTo() {
    return to;
  }
}
