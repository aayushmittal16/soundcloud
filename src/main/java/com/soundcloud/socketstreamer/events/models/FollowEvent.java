package com.soundcloud.socketstreamer.events.models;

import com.soundcloud.socketstreamer.clients.Client;
import com.soundcloud.socketstreamer.clients.RegisteredClients;
import com.soundcloud.socketstreamer.users.User;
import com.soundcloud.socketstreamer.users.UserDb;

public class FollowEvent extends Event {

  private int from;
  private int to;

  public FollowEvent(int sequence, String payload, int from, int to) {
    super(sequence, payload);
    this.from = from;
    this.to = to;
  }

  @Override
  public void apply() {
    UserDb db = UserDb.getInstance();
    User user;
    if (!db.contains(to)) {
      db.add(to);
    }
    user = db.get(to);
    user.addFollower(from);
    Client client = RegisteredClients.getInstance().getClient(to);
    if (client != null) {
      client.write(getPayload());
    }
  }

  public int getFrom() {
    return from;
  }

  public int getTo() {
    return to;
  }
}
