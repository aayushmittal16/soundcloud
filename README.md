# Follower Maze

Follower Maze starts running by accepting connections in `ClientSocketListener` via socket on a configured port in a thread. All connected clients are registered in a registry `RegisteredClients`. Another socket is opened in `EventSourceSocketListener` on a configured port to accept a connection from an event source that sends streams of data to be sorted and sent to connected clients.

The queueing and processing part is done by `EventReceiver` that uses `EventParser` to parse payload data into their respective `Event` models and then call their `apply` methods in a sorted manner. Clients associated with these events are written `payload` data to them.

Once all of the events have been processed, sockets time out based on configured value due to inactivity and the application shuts down.

The codebase has been formatted using [Google Java Format](https://github.com/google/google-java-format) plugin

This project has been built using `JDK 7`. Using `JDK 8+` can already help remove a lot of boilerplate code but I wasn't sure if that is allowed since it is suggested to execute test JAR on `JDK 7` as well.

Logging could be much better by using a third party implementation such as [SLF4J](https://www.slf4j.org). Java logging is severely limited in functionality. Without complex configurations, it reformats numbers with commas, doesn't support more than one parameter for message unless multiple parameters are put together in an array. Creating a custom logging layer to avoid the said issues creates another issue of using the layer's class name and method. Overall, logging could be far more elegant but I ran out of time to deal with it.

## Requirements

1. `JDK 7+` to work with the project

2. [Maven](https://maven.apache.org/) to add dependencies to the project, provide plugins to execute the project, and run tests.

## Dependencies

* `PowerMock Module JUnit 4` PowerMock support for JUnit 4.x
* `PowerMock` A library to provide powerful mocking ability for unit testing.

## Usage

To execute tests, `Maven` is required. Following command executes the tests:
```
mvn clean test
```

Run the application with the following command:
```
mvn exec:java
mvn exec:java -Dexec.args="logLevel FINE timeout 10000"
```

It is possible to run the application without `Maven`. Use the pre-built jar:
```
java -jar application.jar
java -jar application.jar eventListenerPort 9091 clientListenerPort 9092
```
Default logging mode is `INFO`. For logging everything, enable `FINE` mode for traceability:
```
java -jar application.jar logLevel FINE
```

### Supported Parameters:

| Parameters            | Type                           | Default      |
|-----------------------|--------------------------------|--------------|
|logLevel               | String - INFO, SEVERE, FINE    | INFO         |
|eventListenerPort      | Integer                        | 9090         |
|clientListenerPort     | Integer                        | 9099         |
|concurrencyLevel       | Integer                        | 100          |
|numberOfUsers          | Integer                        | concurrencyLevel*10        |
|timeout                | Integer                        | 20000 (in ms)|
|maxEventSourceBatchSize| Integer                        | 100          |

### Notes:
* Log level `FINE` is a replacement for `DEBUG` as Java default logger does not have a `DEBUG` level defined.
* `randomSeed` parameter has no use in this application
* `logInterval` is not supported by `java.util.logging`
* `totalEvents` configuration provides no benefit in this implementation

## Stress tests
Stress tests are based on customizing one parameter at a time:

| Custom Parameters      | Input 1| Result 1| Input 2| Result 2| Input 3| Result 3|
|------------------------|--------|---------|--------|---------|--------|---------|
| totalEvents            | 10^7   | 177s    | 10^6   | 18s     | 10^8   | 1890s   |
| numberOfUsers          | 1000   | 177s    | 100    | 81s     | 10^4   | 1829s   |
| concurrencyLevel       | 100    | 177s    | 10     | 31s     | 1000   | 7300s   |
| maxEventSourceBatchSize| 100    | 177s    | 10     | 209s    | 1000   | 185s    |

Switching log level from `INFO` to `FINE` doubles the runtime for the above cases.

Results can vary significantly based on hardware used. Following are the specs of the machine I used for testing:

* OS: MacOS Mojave
* Processor: 2.6Ghz x 6 cores
* Memory: 32 GB 2400 MHz DDR4
